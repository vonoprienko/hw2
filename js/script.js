var addDynamicNode = {
    firstSpan: "\<span class='sometask'><input type='checkbox' id='strikethrough'>",
    inSpanEdit: "\<input id='editTask' class='button blue small' type='button'  value='Edit'/>",
    inSpanDelete: "\<input id='deleteTask' class='button blue small' type='button' value='Delete'/><br></span>"
};


/**
 * Add listener DOMContentLoaded for document element
 */
document.addEventListener('DOMContentLoaded', newTask, false);

/**
 * Represents a newTask event
 */
function newTask() {
    document.getElementById("correct").style.display = 'none';
    elem.addEventListener("click",
        function () {
            var inpObj = document.getElementById("newTask");
            if (inpObj.checkValidity() == false) {
                alert(inpObj.validationMessage);
            } else {
                $("div#records").prepend(addDynamicNode.firstSpan + inpObj.value + addDynamicNode.inSpanEdit + addDynamicNode.inSpanDelete);
            }
            document.getElementById("newTask").value = "";
        });
}

/**
 * Represents a event "click" on parent node
 * Check which key is pressed and executes the required functionality
 */
$("#records").on("click", "span",
    function (e) {
        var hideAndShow = document.getElementById("hideshow");
        var newTaskInput = document.getElementById("newTask");
        var correctButton = document.getElementById("correct");
        //console.log(e);

        //if click on checkbox - decor or undecor text style
        if (e.target.type === "checkbox") {
            if ($(this).css("text-decoration") !== "none") {
                $(this).css("text-decoration", "");
                if ($('#strikethrough').length > 0) {
                    hideAndShow.value = "Hide all";
                }
            } else {
                $(this).css("text-decoration", "line-through");
                hideAndShow.value = "Hide all";
            }
            //if click on button
        } else if (e.target.type === "button") {
            //if edit button - send text to input and show correct submit button
            //for firefox in input set default text
            if (e.target.id == "editTask") {
                var el = e.currentTarget;
                if (e.currentTarget.innerText !== undefined) {
                    newTaskInput.value = e.currentTarget.innerText;
                } else {
                    newTaskInput.value = "Edit this field and press correct for change value"
                }
                correctButton.style.display = 'inline';
                /**
                 * Add listener for correcy button
                 */
                correct.addEventListener('click',
                    function () {
                        var inpObj = newTaskInput;
                        if (inpObj.checkValidity() == false) {
                            alert(inpObj.validationMessage);
                        } else {
                            //console.log(el);
                            el.outerHTML = addDynamicNode.firstSpan + inpObj.value + addDynamicNode.inSpanEdit + addDynamicNode.inSpanDelete
                        }
                        correctButton.style.display = 'none';
                        newTaskInput.value = "";
                    }
                );
            } else if (e.target.id == "deleteTask") {
                var elThis = this;
                console.log(elThis);
                elThis.parentNode.removeChild(elThis);

                //if no tasks - property set to default
                if ($("p.sometask").length === 0) {
                    correctButton.style.display = 'none';
                    hideAndShow.value = "Hide all";
                }
            }
        }
    }
);

/**
 * Add listener for hideshow element
 */
hideshow.addEventListener("click", hideAndShowAll, false);

/**
 * Represents a hideshow event
 */
function hideAndShowAll() {
    //find checked checkbox
    var findChecked = $('span.sometask:has(input:checked)');

    // define hiddenFields {number} value that will say us showing or not tasks
    var hiddenFields = findChecked.filter(
        function () {
            return $(this).css('display') != "none";
        }
    ).each(function () {
            $(this).hide();
            document.getElementById("hideshow").value = "Show all";

        }).length;
    if (hiddenFields == 0) {
        findChecked.filter(
            function () {
                return $(this).css('display') == "none";
            }
        ).each(function () {
                $(this).show();
                document.getElementById("hideshow").value = "Hide all";
            });
    }
}

